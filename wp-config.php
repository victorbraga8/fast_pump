<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'fast_pump' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&$)%!-|G>hLZ0]K]f,!KKqwrNo2j-=A#pm>+EoN:UlClxTvWYKJm1?w@Pgy(2y>`' );
define( 'SECURE_AUTH_KEY',  ' 3j{7qk4;F=SYRFi@#Ec3^XkI^c D^5.}J8[t?^b[kiTl,ni6-1|3/L2`EobPc5h' );
define( 'LOGGED_IN_KEY',    'U!,QDkd{Oi[=},59 &iHD6O4Eo9jA<5Igg&SKm(Q-7;U+SiIbO^kQ<Yjb.oAbRh9' );
define( 'NONCE_KEY',        ')Q`xm-.[vm+=gpOm?vlm_/tcQbX{V?2.r=g^8c0y+AX,jlxSZsEji5A2yNsy4,0k' );
define( 'AUTH_SALT',        'H~1?*CMt(cCa7XM?ULM`/?E!P!Yn16:]Hh fo,0(}U}?5xQTIp-g{ySl7M8^#mhc' );
define( 'SECURE_AUTH_SALT', '%y~5v?AW[o &<uJ(i>EE^Xm[w`;.-fo [vip%T_c:K;]z&F75x]E96AEY^t>WfuY' );
define( 'LOGGED_IN_SALT',   'u</;.lW5:B_hf=0lg^gw`~yTgCwB7cO`^ys0!!.lT6B{^pH$5#i%~5Wiu8N+/sO#' );
define( 'NONCE_SALT',       'Y=9WRypvzYe*OdgC}*~EW.RRYlgU.}iBx+QV-`wg/IRq}_MTQ{UcI^JXfY/hY;hH' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
